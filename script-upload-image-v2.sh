#!/usr/bin/env bash

# global variable
org_dir=source
out_dir=destination
manifest=imagestream
idfile=`date +%y%m%d%H%I`

if [ "$(oc version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

#check yq is exists or not

if [ ! -f "/usr/local/bin/yq" ]; then
    echo "yq is not exists"
    echo "yq installing ....."
    wget https://github.com/mikefarah/yq/releases/download/v4.23.1/yq_linux_amd64.tar.gz
    tar -zxvf yq_linux_amd64.tar.gz;sudo mv yq_linux_amd64 /usr/local/bin/yq
fi

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
    mkdir tmp -p
  if [ ! -d "$org_dir/$ns" ]; then
      mkdir $org_dir/$ns -p
  fi
  if [ ! -d "$out_dir/$ns" ]; then
      mkdir $out_dir/$ns -p
  fi
    mnfst=`oc get $manifest -n $ns --no-headers | awk '{print $1}'`
    for m in $mnfst
    do
        echo "Download image.."
        RAW=`cat  $org_dir/$ns/$m-$manifest.yaml | yq '.spec.template.spec.containers[].image' | cut -d@ -f 1 | sed "s/image-registry.openshift-image-registry.svc:5000/registry-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id"`
        IMAGE=`echo $RAW:latest`
        echo $IMAGE > tmp/image-$(date +%y-%m-%d).txt
        docker pull $IMAGE
    done
done