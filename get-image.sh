#!/usr/bin/env bash

count=`cat sample/image-sample.txt | grep -v NAME | grep image-registry | wc -l`
repo=`cat sample/image-sample.txt | grep -v NAME | grep image-registry | awk '{print $3}' | cut -d "@" -f 1`
image=`cat sample/image-sample.txt | grep -v NAME | grep image-registry | awk '{print $2}'`

for((i=1;i<=$count;i++)); do
    echo `cat sample/image-sample.txt | grep -v NAME | grep image-registry | awk '{print $3}' | cut -d "@" -f 1 | head -$i | tail -1`:latest
done
