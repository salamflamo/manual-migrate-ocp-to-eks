#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-upload-image.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=imagestream
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
host=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id
ecr_host=670012318318.dkr.ecr.ap-southeast-3.amazonaws.com

if [ -z "$($oc_cmd version)" ]; then
    echo "oc does not exists"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
    mkdir tmp -p
  if [ ! -d "$org_dir" ]; then
      mkdir $org_dir -p
  fi
  if [ ! -d "$out_dir" ]; then
      mkdir $out_dir -p
  fi
    echo "Login into ECR"
    aws ecr get-login-password --region ap-southeast-3 | sudo docker login --username AWS --password-stdin $ecr_host
    file=`ls $out_dir | grep downloaded-images | tail -1`
    images=`cat $out_dir/$file`
    for image in $images; do
        tag=latest
        echo "Tagging image"
        sudo docker tag $host/$image:$tag $ecr_host/$image:$tag
        echo "Create repository"
        aws ecr create-repository --repository-name $image --image-scanning-configuration scanOnPush=true
        echo "Upload image"
        sudo docker push $ecr_host/$image:$tag
        echo "Delete on source"
        sudo docker rmi $host/$image:$tag
        sudo docker rmi $ecr_host/$image:$tag
    done
done