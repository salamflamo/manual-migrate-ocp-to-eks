#!/bin/bash
org_dir=./original/namespace
dest_dir=./destination/namespace

mkdir $org_dir -p
mkdir $dest_dir -p

for ns in $(kubectl get ns --no-headers | grep -v "kube-" | awk '{print $1}')
do
    echo "Get Namespace $ns"
    kubectl get ns -o yaml > $org_dir/$ns.yaml
done