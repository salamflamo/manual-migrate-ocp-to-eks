#!/bin/bash
ns_list=$1

if [[ -z "${ns_list}" ]]; then
    echo "How to use : $0 /path/to/file_ns_list.txt"
    exit 1
fi

yq_cmd=./yq

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "yq installing ....."
    wget https://github.com/mikefarah/yq/releases/download/v4.23.1/yq_linux_amd64.tar.gz
    tar -zxvf yq_linux_amd64.tar.gz;mv yq_linux_amd64 yq
    chmod +x yq
fi

manifest=horizontalpodautoscaler

for ns in $(cat $ns_list)
do
    org_dir=./original/$ns/$manifest
    dest_dir=./destination/$ns/$manifest

    mkdir $org_dir -p
    mkdir $dest_dir -p
    echo "Get From Namespace $ns"
    
    for file in $(kubectl get $manifest -n $ns --no-headers | awk '{print $1}')
    do
        echo "Get $manifest $file in namespace $ns ..."
        kubectl get $manifest -n $ns $file -o yaml > $org_dir/$file.yaml
        cp $org_dir/$file.yaml $dest_dir/$file.yaml
        echo "Modify $manifest $file in namespace $ns ..."
        $yq_cmd eval -i '.spec.replicas = 0' $dest_dir/$file.yaml
        $yq_cmd eval -i '.spec.minReplicas = 1' $dest_dir/$file.yaml
        $yq_cmd eval -i 'del(.status)' $dest_dir/$file.yaml
    done
done