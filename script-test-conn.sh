#!/bin/bash
file=$1
if [[ -z $file ]];then
        echo "please add list of ip in file, like this $0 list-ip-port.txt"
        exit 1
fi


echo "$(date) test begining"
echo "############################"
while IFS= read -r line
do
        arr=($line)
        echo "Test for ip ${arr[0]} port ${arr[1]} site ${arr[2]}"
        #sudo nping -c 1 --tcp -p ${arr[1]} ${arr[0]}
        #nc -zv ${arr[0]} ${arr[1]}
        sudo nmap -Pn --traceroute -n -p ${arr[1]} ${arr[0]}
        #traceroute ${arr[0]}
        #echo -----------------------------------------------------
        echo "result_ip: ${arr[0]} site ${arr[2]}"
        echo ----------------------------------------------------
        echo
done< <(cat $file)
sleep 1
echo "$(date) test end"
echo "///////////////////////////////"
