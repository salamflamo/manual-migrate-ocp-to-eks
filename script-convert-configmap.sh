#!/usr/bin/env bash

if [ "$(oc version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

#check yq is exists or not

if [ ! -f "/usr/local/bin/yq" ]; then
    echo "yq is not exists"
    echo "yq installing ....."
    wget https://github.com/mikefarah/yq/releases/download/v4.23.1/yq_linux_amd64.tar.gz
    tar -zxvf yq_linux_amd64.tar.gz;sudo mv yq_linux_amd64 /usr/local/bin/yq
fi


# global variable
org_dir=source
out_dir=destination
manifest=configmap

echo "Process begin ...."
namespaces=`oc get namespace -A --no-headers | awk '{print $1}' | grep -v openshift`
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
  if [ ! -d "$org_dir/$ns" ]; then
      mkdir $org_dir/$ns -p
  fi
  if [ ! -d "$out_dir/$ns" ]; then
      mkdir $out_dir/$ns -p
  fi
    mnfst=`oc get $manifest -n $ns --no-headers | awk '{print $1}'`
    for m in $mnfst
    do
       echo "Collecting manifest $m.."
        oc get $manifest $m -o yaml -n $ns > $org_dir/$ns/$m-$manifest.yaml
        cp $org_dir/$ns/$m-$manifest.yaml $out_dir/$ns/$m-$manifest.yaml

        echo "Converting $manifest $m..."
        yq eval -i 'del(.metadata.annotations)' $out_dir/$ns/$m-$manifest.yaml
        yq eval -i 'del(.metadata.creationTimestamp)' $out_dir/$ns/$m-$manifest.yaml
        yq eval -i 'del(.metadata.managedFields)' $out_dir/$ns/$m-$manifest.yaml
        yq eval -i 'del(.metadata.resourceVersion)' $out_dir/$ns/$m-$manifest.yaml
        yq eval -i 'del(.metadata.selfLink)' $out_dir/$ns/$m-$manifest.yaml
        yq eval -i 'del(.metadata.uid)' $out_dir/$ns/$m-$manifest.yaml

    done
done