#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-download-image.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=imagestream
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
idfile=`date +%y%m%d%H%I`
host=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id
OCPUSER=testuser

# global variable


if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Login to container registry"
cat << EOF | sudo tee -a /etc/docker/daemon.json
{
  "insecure-registries" : ["$host"]
}
EOF
sudo systemctl restart docker.service
sudo docker login -u $OCPUSER -p `$oc_cmd whoami -t` $host

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
    mkdir tmp -p
  if [ ! -d "$org_dir" ]; then
      mkdir $org_dir -p
  fi
  if [ ! -d "$out_dir" ]; then
      mkdir $out_dir -p
  fi
    $oc_cmd get $manifest -n $ns --no-headers > $org_dir/images-$idfile.txt
    tot=`cat $org_dir/images-$idfile.txt | wc -l`
    for ((i=1;i<=$tot;i++)); do
        image=`cat $org_dir/images-$idfile.txt | tail -$i | head -1 | awk '{print $1}'`
        tag=`cat $org_dir/images-$idfile.txt | tail -$i | head -1 | awk '{print $3}'`
        echo "download image $ns/$image:$tag"
        echo $ns/$image >> $out_dir/downloaded-images-$idfile.txt
        sudo docker pull $host/$ns/$image:latest
    done
done