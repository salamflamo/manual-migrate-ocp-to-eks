#!/bin/bash

pip3 install pyyaml
mkdir -p source/deployment
mkdir -p source/service
mkdir -p source/ingress
mkdir -p source/configmap
mkdir -p source/secrets
mkdir -p destination/deployment/sz
mkdir -p destination/deployment/dmz
mkdir -p destination/service/sz
mkdir -p destination/service/dmz
mkdir -p destination/ingress/sz
mkdir -p destination/ingress/dmz
mkdir -p destination/configmap/sz
mkdir -p destination/configmap/dmz
mkdir -p destination/secrets/dmz
mkdir -p destination/secrets/sz

DEPLOYMENT=$(kubectl get deployment --no-headers | awk '{print $1}')
SERVICE=$(kubectl get deployment --no-headers | awk '{print $1}')
SECRET=$(kubectl get deployment --no-headers | awk '{print $1}')
CONFIGMAP=$(kubectl get deployment --no-headers | awk '{print $1}')
ROUTES=$(kubectl get deployment --no-headers | awk '{print $1}')
SOURCE_FOLDER="source/"
DESTINATION_FOLDER="destination/"

for d in $DEPLOYMENT; do
    kubectl get deployment $d -o yaml > $SOURCE_FOLDER/$d
done

for e in $SERVICE; do
    kubectl get deployment $d -o yaml > $SOURCE_FOLDER/$e
done

for f in $SECRET; do
    kubectl get deployment $d -o yaml > $SOURCE_FOLDER/$f
done

for g in $CONFIGMAP; do
    kubectl get deployment $d -o yaml > $SOURCE_FOLDER/$g
done

for h in $ROUTES; do
    kubectl get deployment $d -o yaml > $SOURCE_FOLDER/$h
done