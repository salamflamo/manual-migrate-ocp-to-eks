
import os
from random import sample
import yaml

source_folder="source/deployment"
dest_folder_sz="destination/deployment/sz"
dest_folder_dmz="destination/deployment/dmz"

def convertToK8s_SZ(yamlcontent):
    destination_data = {'apiVersion':'','kind':'','metadata':{},'spec':{'template':{'spec':{'containers':{'name':{}}},'metadata':{'labels':{}}}}}
    for data in yaml.load_all(yamlcontent, Loader=yaml.FullLoader):
        destination_data['kind'] = data['kind']
        destination_data['apiVersion'] = 'apps/v1'
        destination_data['metadata']['name'] = data['metadata']['name']+"-sz"
        destination_data['metadata']['namespace'] = data['metadata']['namespace']
        destination_data['metadata']['labels'] = data['metadata']['labels']
        destination_data['spec']['strategy'] = data['spec']['strategy']
        destination_data['spec']['replicas'] = data['spec']['replicas']
        destination_data['spec']['selector'] = data['spec']['selector']
        destination_data['spec']['template']['metadata'] = data['spec']['template']['metadata']
        destination_data['spec']['template']['spec'] = data['spec']['template']['spec']
        destination_data['spec']['selector']['app'] = destination_data['spec']['selector']['app']+'-sz'
        destination_data['metadata']['labels']['app'] = data['metadata']['labels']['app']+'-sz'
        destination_data['spec']['template']['metadata']['labels']['app'] = destination_data['spec']['template']['metadata']['labels']['app']+'-sz'
    return destination_data

# def convertToK8s_DMZ(yamlcontent):
#     destination_data = {'apiVersion':'','kind':'','metadata':{},'spec':{'template':{'spec':{'containers':{'name':{}}},'metadata':{'labels':{}}}}}
#     for data in yaml.load_all(yamlcontent, Loader=yaml.FullLoader):
#         destination_data['kind'] = data['kind']
#         destination_data['apiVersion'] = 'apps/v1'
#         destination_data['metadata']['name'] = data['metadata']['name']+"-dmz"
#         destination_data['metadata']['namespace'] = data['metadata']['namespace']+"-dmz"
#         destination_data['metadata']['labels'] = data['metadata']['labels']
#         destination_data['spec']['strategy'] = data['spec']['strategy']
#         destination_data['spec']['replicas'] = data['spec']['replicas']
#         destination_data['spec']['selector'] = data['spec']['selector']
#         destination_data['spec']['template']['metadata'] = data['spec']['template']['metadata']
#         destination_data['spec']['template']['spec'] = data['spec']['template']['spec']
#         destination_data['spec']['selector']['app'] = destination_data['spec']['selector']['app']+'-dmz'
#         destination_data['metadata']['labels']['app'] = data['metadata']['labels']['app']+'-dmz'
#         destination_data['spec']['template']['metadata']['labels']['app'] = destination_data['spec']['template']['metadata']['labels']['app']+'-dmz'
#         # destination_data['spec']['template']['metadata']['annotations'].pop(0)
#     return destination_data


for filename in os.listdir(source_folder):
    oldfile = open(source_folder+'/'+filename,'r+')
    filecontent = oldfile.read()
    oldfile.close()
    converted_sz = convertToK8s_SZ(filecontent)
    newfilesz = open(dest_folder_sz+'/'+filename+'-sz','w+')
    newfilesz.write(str(converted_sz))
    newfilesz.close()
    # converted_dmz = convertToK8s_DMZ(filecontent)
    # newfiledmz = open(dest_folder_dmz+'/'+filename+'-dmz','w+')
    # newfiledmz.write(str(converted_dmz))
    # newfiledmz.close()