# migrate OCP deploymentConfig manifest to Kubernetes

## logical flow

1. run dump-config.sh to install python dependencies and create needed folder. this need to be done in every environment
2. dump all dependencies into respective folder
3. run migrate-workload.py to migrate the deploymentConfig from source/deployment folder. and print it into destination/deployment