# OCP Registries to ECR migration
## logical flow to migrate from OCP container registries to ECR

```
1. pull from OCP registries
2. login to aws ecr
3. tag the pulled image to ecr
4. push the tagged image to ECR
```

## Notes:
- seed_files.txt must contains list of OCP Registries container name