#!/bin/bash

NAMESPACE=""
IMAGENAME=""
ECR_URL=""
OCP_URL="image-registry.openshift-image-registry.svc:5000"

while read file; do
    docker pull $IMAGENAME
    ## login to aws ecr
    aws ecr-public get-login-password --region us-east-1 | docker login --username AWS --password-stdin public.ecr.aws/v6s8g7r9
    docker tag $IMAGENAME $ECR_URL/$IMAGENAME
    docker push $ECR_URL/$IMAGENAME
done < seed_files.txt


## SAMPLE OCP IMAGE VALUE
## quay.io/openshift-release-dev/ocp-v4.0-art-dev@sha256:e353b8358c9a33971550fc1dbb2ee4d1d20e8b809e7f44a2f785a7404c3f277c