#!/usr/bin/env bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-routes.log"
    exit 1
fi

# global variable
example=original/ingress.yaml
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=route
service_dir=source/service
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest


if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
  if [ ! -d "$org_dir" ]; then
      mkdir $org_dir -p
  fi
  if [ ! -d "$out_dir" ]; then
      mkdir $out_dir -p
  fi
    mnfst=`$oc_cmd get $manifest -n $ns --no-headers | awk '{print $1}'`
    for m in $mnfst
    do
        echo "Collecting manifest $manifest $m.."
        $oc_cmd get $manifest $m -o yaml -n $ns > $org_dir/$m-$manifest.yaml
        
        echo "Converting $manifest $m..."
        HOST=`cat $org_dir/$m-$manifest.yaml | $yq_cmd .spec.host`
        SERVICE=`cat $org_dir/$m-$manifest.yaml | $yq_cmd .spec.to.name`
        check_port_route=`cat $org_dir/$m-$manifest.yaml | $yq_cmd .spec.port.targetPort`
        NAME=`cat $org_dir/$m-$manifest.yaml | $yq_cmd .metadata.name`
        PORT=8080
        if [ $check_port_route == "null" ];then
            ports=`$oc_cmd get service $SERVICE -n $ns -o yaml | $yq_cmd .spec.ports[].port | wc -l`
            if [ $ports == 1 ];then
                PORT=`$oc_cmd get service $SERVICE -n $ns -o yaml | $yq_cmd .spec.ports[].port`
                cat $example | sed -e "s/namespace: <namespace>/namespace: $ns/g" | sed -e "s/name: <name>/name: $NAME/g" | sed -e "s/host: <host>/host: $HOST/g" | sed -e "s/name: <service>/name: $SERVICE/g" | sed -e "s/number: <port>/number: $PORT/g" | tee -a $out_dir/$m-$manifest.yaml
            else
                PORT=`$oc_cmd get service $SERVICE -n $ns -o yaml | $yq_cmd .spec.ports[].port | head -1`
                cat $example | sed -e "s/namespace: <namespace>/namespace: $ns/g" | sed -e "s/name: <name>/name: $NAME/g" | sed -e "s/host: <host>/host: $HOST/g" | sed -e "s/name: <service>/name: $SERVICE/g" | sed -e "s/number: <port>/number: $PORT/g" | tee -a $out_dir/$m-$manifest.yaml
            fi
        else
            if [[ $check_port_route =~ ^-?[0-9]+$ ]];then
                PORT=$check_port_route
                cat $example | sed -e "s/namespace: <namespace>/namespace: $ns/g" | sed -e "s/name: <name>/name: $NAME/g" | sed -e "s/host: <host>/host: $HOST/g" | sed -e "s/name: <service>/name: $SERVICE/g" | sed -e "s/number: <port>/number: $PORT/g" | tee -a $out_dir/$m-$manifest.yaml
            else 
                PORT=$check_port_route
                cat $example | sed -e "s/namespace: <namespace>/namespace: $ns/g" | sed -e "s/name: <name>/name: $NAME/g" | sed -e "s/host: <host>/host: $HOST/g" | sed -e "s/name: <service>/name: $SERVICE/g" | sed -e "s/number: <port>/name: $PORT/g" | tee -a $out_dir/$m-$manifest.yaml
            fi
        fi
        #labels
    done
done