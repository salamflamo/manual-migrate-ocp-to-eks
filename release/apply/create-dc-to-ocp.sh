#!/usr/bin/env bash

# global variable
oc_cmd=oc
yq_cmd=yq
namespaces=sit11-esb
manifest=deploymentconfig

echo "Process begin ...."

mnfst=$(ls $manifest)
for m in $mnfst
do
    echo "Apply $manifest $m..."
    $yq_cmd eval -i 'del(.metadata.annotations)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.generation)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.creationTimestamp)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.managedFields)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.resourceVersion)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.selfLink)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.uid)' $manifest/$m
done
