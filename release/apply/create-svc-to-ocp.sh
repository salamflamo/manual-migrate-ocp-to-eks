#!/usr/bin/env bash

# global variable
oc_cmd=oc
yq_cmd=yq
namespaces=sit11-esb
manifest=service

echo "Process begin ...."

mnfst=$(ls $manifest)
for m in $mnfst
do
    echo "Apply $manifest $m..."
    $yq_cmd eval -i 'del(.metadata.annotations)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.generation)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.creationTimestamp)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.managedFields)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.resourceVersion)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.selfLink)' $manifest/$m
    $yq_cmd eval -i 'del(.metadata.uid)' $manifest/$m
    $yq_cmd eval -i 'del(.spec.clusterIP)' $manifest/$m
    $yq_cmd eval -i 'del(.spec.clusterIPs)' $manifest/$m
    $yq_cmd eval -i 'del(.spec.externalIPs)' $manifest/$m
    $yq_cmd eval -i 'del(.spec.ports[].nodePort)' $manifest/$m
    $yq_cmd eval -i 'del(.status)' $manifest/$m
done
