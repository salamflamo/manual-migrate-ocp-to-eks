#!/usr/bin/env bash

exlude="
agnosticordervalidation-rv
assistedproductoffer-bs
assistedproductoffer-co
domcallback-co
ordersubmission-bs-v2-metaswitch
personalizedmenu-co-v2
productoffer-bs-x-r
pushnotification-bs
resellerordervalidation-rv-v2
servicebalance-bs
smsmarketplacepostpaid-bs
tcarepin-bs
"
mkdir exclude/from-ocp -p
mkdir exclude/from-convert -p

for dc in $exlude; do
    kubectl get deploy $dc -n sit11-esb -o yaml > exclude/$dc-deploy.yaml
    kubectl get svc $dc -n sit11-esb -o yaml > exclude/$dc-svc.yaml
    file=$dc-deploymentconfig.yaml
    svc=$dc-service.yaml
    mv deploymentconfig/$file exclude/from-convert
    mv service/$svc exclude/from-convert
    # kubectl get ingress $dc -n sit11-esb -o yaml > exclude/$dc-ingress.yaml
    # mv deploymentconfig/$dc-deploymentconfig.yaml exclude/from-ocp/
    # mv service/$dc-service.yaml exclude/from-ocp/
done

for dc in $(kubectl get deploy -n sit11-esb --no-headers | awk '{print $1}' \
    | grep -v  agnosticordervalidation-rv \
    | grep -v  assistedproductoffer-bs \
    | grep -v  assistedproductoffer-co \
    | grep -v  domcallback-co \
    | grep -v  ordersubmission-bs-v2-metaswitch \
    | grep -v  personalizedmenu-co-v2 \
    | grep -v  productoffer-bs-x-r \
    | grep -v  pushnotification-bs \
    | grep -v  resellerordervalidation-rv-v2 \
    | grep -v  servicebalance-bs \
    | grep -v  smsmarketplacepostpaid-bs \
    | grep -v  tcarepin-bs);do
    echo "Delete dc $dc"
    kubectl delete deploy $dc -n sit11-esb
done

for svc in $(kubectl get service -n sit11-esb --no-headers | awk '{print $1}' \
    | grep -v  agnosticordervalidation-rv \
    | grep -v  assistedproductoffer-bs \
    | grep -v  assistedproductoffer-co \
    | grep -v  domcallback-co \
    | grep -v  ordersubmission-bs-v2-metaswitch \
    | grep -v personalizedmenu-co-v2 \
    | grep -v  productoffer-bs-x-r \
    | grep -v  pushnotification-bs \
    | grep -v  resellerordervalidation-rv-v2 \
    | grep -v  servicebalance-bs \
    | grep -v  smsmarketplacepostpaid-bs \
    | grep -v  tcarepin-bs);do
    echo "Delete svc $svc"
    kubectl delete svc $svc -n sit11-esb
done

# kubectl get ingress -n sit11-esb \
# | grep -v  agnosticordervalidation-rv \
# | grep -v  assistedproductoffer-bs \
# | grep -v  assistedproductoffer-co \
# | grep -v  domcallback-co \
# | grep -v  ordersubmission-bs-v2-metaswitch \
# | grep -v personalizedmenu-co-v2 \
# | grep -v  productoffer-bs-x-r \
# | grep -v  pushnotification-bs \
# | grep -v  resellerordervalidation-rv-v2 \
# | grep -v  servicebalance-bs \
# | grep -v  smsmarketplacepostpaid-bs \
# | grep -v  tcarepin-bs
