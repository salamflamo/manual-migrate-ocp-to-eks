#!/bin/bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
kube_cmd=./kubectl
namespaces=$1
manifest=serviceaccount
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest

sas=`ls $out_dir | sort`

for sa in $sas
do
    name=`cat $out_dir/$sa | $yq_cmd .metadata.name`
    $kube_cmd create $manifest $name -n $namespaces
done