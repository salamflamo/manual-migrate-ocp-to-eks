#!/usr/bin/env bash
oc_cmd=./oc
yq_cmd=./yq

if [ -z "$($oc_cmd version)" ]; then 
    echo "Command not found"
    wget https://mirror.openshift.com/pub/openshift-v4/clients/ocp/4.7.9/openshift-client-linux-4.7.9.tar.gz
    tar -zxvf openshift-client-linux-4.7.9.tar.gz
fi

if [ -z "$(docker -v)" ]; then 
    echo "Command not found"
    sudo yum install docker -y
    sudo usermod -aG docker ec2-user
    sudo systemctl enable docker.service --now
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "yq installing ....."
    wget https://github.com/mikefarah/yq/releases/download/v4.23.1/yq_linux_amd64.tar.gz
    tar -zxvf yq_linux_amd64.tar.gz;mv yq_linux_amd64 yq
fi