#!/usr/bin/env bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-deploymentconfig.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=deploymentconfig
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest


if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
  if [ ! -d "$org_dir" ]; then
      mkdir $org_dir -p
  fi
  if [ ! -d "$out_dir" ]; then
      mkdir $out_dir -p
  fi
    mnfst=`$oc_cmd get $manifest -n $ns --no-headers | awk '{print $1}'`
    for m in $mnfst
    do
        echo "Collecting manifest $manifest $m.."
        $oc_cmd get $manifest $m -o yaml -n $ns > $org_dir/$m-$manifest.yaml
        cp $org_dir/$m-$manifest.yaml $out_dir/$m-$manifest.yaml
        # RAW=`cat $org_dir/$m-$manifest.yaml | $yq_cmd '.spec.template.spec.containers[].image' | cut -d@ -f 1 | sed "s/image-registry.openshift-image-registry.svc:5000/670012318318.dkr.ecr.ap-southeast-3.amazonaws.com/g"`
        # export IMAGE=`echo $RAW:latest`
        # substr=":latest"
        # if [[ $RAW == *"$substr"* ]]; then
        #     export IMAGE=`echo $RAW`  
        # fi
        RAW=`cat $org_dir/$m-$manifest.yaml | $yq_cmd '.spec.template.spec.containers[].image' | sed "s/image-registry.openshift-image-registry.svc:5000/670012318318.dkr.ecr.ap-southeast-3.amazonaws.com/g"`
        export IMAGE=`echo $RAW`
        cat $org_dir/$m-$manifest.yaml | $yq_cmd .spec.selector | sed "s/://g" | tee -a tmp-$m.txt

        echo "Converting $manifest $m..."
        $yq_cmd eval -i 'del(.metadata.annotations)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.generation)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.creationTimestamp)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.managedFields)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.resourceVersion)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.selfLink)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.uid)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.test)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.triggers)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.selector)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.strategy.activeDeadlineSeconds)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.strategy.rollingParams)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.strategy.resources)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.template.metadata.annotations)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.status)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i '.spec.template.spec.containers[].image = strenv(IMAGE)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i '.kind = "Deployment"' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i '.apiVersion = "apps/v1"' $out_dir/$m-$manifest.yaml
        strategy=`$yq_cmd eval '.spec.strategy.type' $out_dir/$m-$manifest.yaml`
        if [[ "${strategy}" == "Rolling" ]]; then
            $yq_cmd eval -i '.spec.strategy.type = "RollingUpdate"' $out_dir/$m-$manifest.yaml
        else
            $yq_cmd eval -i '.spec.strategy.type = "Recreate"' $out_dir/$m-$manifest.yaml
        fi

        $yq_cmd -i eval '
            .spec += {
              "selector": {
                "matchLabels": {}
            }
        }' $out_dir/$m-$manifest.yaml

        while IFS= read -r line
        do
            arr=($line)
            export key=${arr[0]}
            export value=${arr[1]}
            $yq_cmd -i eval '.spec.selector.matchLabels += {strenv(key): strenv(value)}' $out_dir/$m-$manifest.yaml
        done< <(cat tmp-$m.txt)
        rm -f tmp-$m.txt
    done
done