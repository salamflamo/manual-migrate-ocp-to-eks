#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-download-image.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=imagestreamtag
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
idfile=`date +%y%m%d%H%I`
host=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id
host_int=image-registry.openshift-image-registry.svc:5000
host_ecr=670012318318.dkr.ecr.ap-southeast-3.amazonaws.com
OCPUSER=testuser

# global variable


if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Login to container registry"
sudo rm -f /etc/docker/daemon.json
cat << EOF | sudo tee -a /etc/docker/daemon.json
{
  "insecure-registries" : ["$host"]
}
EOF
sudo systemctl restart docker.service

echo "$(date) Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
    mkdir tmp -p
    if [ ! -d "$org_dir" ]; then
        mkdir $org_dir -p
    fi
    if [ ! -d "$out_dir" ]; then
        mkdir $out_dir -p
    fi

    $oc_cmd get $manifest -n $ns --no-headers | awk '{print $1,$2,$3}' > $org_dir/$manifest-$idfile.txt
    $oc_cmd get imagestream -n $ns --no-headers | awk '{print $1,$2,$3}' > $org_dir/imagestream-$idfile.txt
    dcdir=source/$namespaces/deploymentconfig
    dcexp=destination/$namespaces/deploymentconfig

    if [ -d $dcdir ]; then
        if [[ $tot == 0 ]];then
            echo "Please collect deployment first"
            exit 1
        fi
    else
            echo "Please collect deployment first"
            exit 1
    fi

    for dc in $(ls $dcdir);do
        img_dc=`cat $dcdir/$dc | $yq_cmd '.spec.template.spec.containers[].image'`
        for img in $img_dc;do
            sudo docker login -u $OCPUSER -p `$oc_cmd whoami -t` $host
            export image=`echo $img | sed "s/$host_int/$host/g"`
            export tag="latest"
            export img_ecr="-"
            export ident="sha256:"
            if [[ $image == *"$ident"* ]]; then
                tag=`cat $org_dir/$manifest-$idfile.txt | grep $img | awk '{print $1}' | cut -d: -f 2 | head -1`
                if [[ -z "${tag}" ]];then                    
                    im=`echo $img | cut -d@ -f 1 | cut -d/ -f 3`
                    i=`cat $org_dir/imagestream-$idfile.txt | grep "$ns/$im"`
                    ig=`echo $i | awk '{print $2}'`
                    for t in $(cat $org_dir/imagestream-$idfile.txt | awk '{print $3}');do 
                        echo $t | sed "s/,/ /g" > tmp-tag.txt;
                        arr=($(cat tmp-tag.txt));
                        len=${#arr[@]};
                        for (( i=0;i<$len;i++ ));do 
                            if [[ "${arr[$i]}" = "latest" ]];then 
                                tag=${arr[$i]};
                            else 
                                tag=${arr[-1]}
                            fi
                        done
                        rm -f tmp-tag.txt
                    done
                    image=`echo $ig:$tag`
                fi
                
            fi
            img_ecr=$image
            if [[ $image == *"$ident"* ]];then
                    untag_img=`echo $img_dc | cut -d@ -f 1`
                    img_conv=`echo $untag_img | sed "s/$host_int/$host_ecr/g"`
                    img_ecr=`echo $img_conv:$tag`
            fi
            echo $img_ecr >> $out_dir/image-upload-$idfile.txt
            echo $image >> $org_dir/image-download-$idfile.txt
            echo ---------------------
            echo "OCP : $image"
            echo "ECR : $img_ecr"
            echo "Pull image $image"
            sudo docker pull $image
            sleep  2s
            sudo docker tag $image $img_ecr
            sleep 2s
            aws ecr get-login-password --region ap-southeast-3 | sudo docker login --username AWS --password-stdin $host_ecr
            echo "Push image $img_ecr"
            sudo docker push $img_ecr
            sleep 2s
            sudo docker rmi $img_ecr --force
        done
    done
done
sudo docker images | awk '{print $3}' | xargs sudo docker rmi
echo "$(date) Process end ...."

