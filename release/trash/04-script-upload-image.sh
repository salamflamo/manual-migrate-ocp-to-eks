#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-upload-image.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=imagestreamtag
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
host_ecr=670012318318.dkr.ecr.ap-southeast-3.amazonaws.com
host=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id

if [ -z "$($oc_cmd version)" ]; then
    echo "oc does not exists"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
    mkdir tmp -p
  if [ ! -d "$org_dir" ]; then
      mkdir $org_dir -p
  fi
  if [ ! -d "$out_dir" ]; then
      mkdir $out_dir -p
  fi
    echo "Login into ECR"
    aws ecr get-login-password --region ap-southeast-3 | sudo docker login --username AWS --password-stdin $host_ecr
    echo "Starting upload image..."
    file=`ls $out_dir | grep image-upload | tail -1`
    images=`cat $out_dir/$file`
    for image in $images; do
        sudo docker push $image
        sudo docker rmi $image
    done

    echo "Starting delete image..."
    file=`ls $org_dir | grep image-download | tail -1`
    images=`cat $org_dir/$file`
    for image in $images; do
        sudo docker rmi $image
    done
done