#!/usr/bin/env bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba file-(dc|rc) | tee -a ns-coba-deploymentconfig.log"
    exit 1
fi

if [ -z $2 ]; then
    echo "What file dude?, look at the example $0 ns-coba file-(dc|rc) | tee -a ns-coba-deploymentconfig.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
file=$2
manifest=deploymentconfig
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
istag_dir=source/$namespaces/imagestreamtag
host_ocp=image-registry.openshift-image-registry.svc:5000
host_ecr=670012318318.dkr.ecr.ap-southeast-3.amazonaws.com
host=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id
ocp_user=22343102

if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo $(date)
echo "From namespace $namespaces .."
if [ ! -d "$org_dir" ]; then
    mkdir $org_dir -p
fi
if [ ! -d "$out_dir" ]; then
    mkdir $out_dir -p
fi

# sudo rm -f /etc/docker/daemon.json
# cat << EOF | sudo tee -a /etc/docker/daemon.json
# {
#   "insecure-registries" : ["$host"]
# }
# EOF
# sudo systemctl restart docker.service

image_ocp=""
image_ecr=""
image_raw=""
tag="notag"
mkdir -p $istag_dir
$oc_cmd get imagestreamtag -n $namespaces --no-headers | awk '{print $1,$2}' > $istag_dir/imagestreamtag.txt

if [ ! -f "$file" ]; then
    echo "Canceling download container image, file doesn't exists"
else
    images=$(cat $file | $yq_cmd '.spec.template.spec.containers[].image')
    for img in $images;do
        tag=$(cat $istag_dir/imagestreamtag.txt | grep $img | awk '{print $1}' | cut -d: -f 2)
        if [[ -z "${tag}" ]]; then
            tag="notag"
        fi
        if [[ "${img}" == *"${host_ocp}"* ]]; then
            image_ocp=$(echo $img | sed "s/$host_ocp/$host/g")
            image_raw=$(echo $img | sed "s/$host_ocp/$host_ecr/g" | cut -d@ -f 1)
            image_ecr=$(echo $image_raw:$tag)
        else
            image_ocp=$img
            image_ecr=$(echo $img | sed "s/$host/$host_ecr/g")
        fi
        img_name=$(echo $image_raw | cut -d/ -f 3)
        echo "$(date) Pull image $image_ocp"
        sudo docker login -u $ocp_user -p $($oc_cmd whoami -t) $host
        sudo docker pull $image_ocp
        sudo docker tag $image_ocp $image_ecr
        aws ecr create-repository --repository-name $namespaces/$img_name --image-scanning-configuration scanOnPush=true
        aws ecr get-login-password --region ap-southeast-3 | sudo docker login --username AWS --password-stdin $host_ecr
        echo "$(date) Push image $image_ecr"
        sudo docker push $image_ecr
        sudo docker images | grep $img_name | awk '{print $3}' | xargs sudo docker rmi --force
    done
fi
