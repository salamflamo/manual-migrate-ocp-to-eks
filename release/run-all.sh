#!/bin/bash
ns=$1
if [[ -z $1 ]];then echo "please add namespace like this $0 ns-coba";exit 1;fi

echo "$(date) Start from namespace $ns"
mkdir log -p
bash 01-script-convert-deploymentconfig-v2.sh $ns | tee -a log/$ns-deploymentconfig.log
bash 03-script-convert-configmap.sh $ns | tee -a log/$ns-cm.log
bash 04-script-convert-secret.sh $ns | tee -a log/$ns-secret.log
bash 05-script-convert-serviceaccount.sh $ns | tee -a log/$ns-sa.log
bash 06-script-convert-service.sh $ns | tee -a log/$ns-service.log
bash 07-script-convert-routes.sh $ns | tee -a log/$ns-routes.log
bash 08-script-convert-horizontalpodautoscaler.sh $ns | tee -a log/$ns-hpa.log
if [ -f "rc-list/$ns.txt" ]; then
        bash 02-script-convert-replicationcontroller-v2.sh $ns rc-list/$ns.txt | tee -a log/$ns-configmap.log 
fi
echo "Finish from namespace $ns"
sleep 30
