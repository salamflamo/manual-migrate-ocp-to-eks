# Prereq before you go with these scripts

1. Install OC client, you can download here https://mirror.openshift.com/pub/openshift-v4/x86_64/clients/ocp/ and choose your desired version.
2. Install AWS CLI, you can download here https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip .
3. Login to OCP using CLI `oc login -u <user> -p <password> https://api.ocp.server:6443` .
4. Configure AWS CLI `aws configure` then copy paste your access key and private key, and set your default region code.
5. Enjoy your ride !.
