#!/usr/bin/env bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-service.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=service
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
source_dep=source/$namespaces/deploymentconfig


if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo "Process begin ...."
for ns in $namespaces;
do
    echo $(date)
    echo "From namespace $ns.."
  if [ ! -d "$org_dir" ]; then
      mkdir $org_dir -p
  fi
  if [ ! -d "$out_dir" ]; then
      mkdir $out_dir -p
  fi
    mnfst=`$oc_cmd get $manifest -n $ns --no-headers | awk '{print $1}'`
    for m in $mnfst
    do
       echo "Collecting manifest $manifest $m.."
        $oc_cmd get $manifest $m -o yaml -n $ns > $org_dir/$m-$manifest.yaml
        cp $org_dir/$m-$manifest.yaml $out_dir/$m-$manifest.yaml
        if [ ! -d "./tmp" ]; then
            mkdir tmp
        fi
        echo "Converting $manifest $m..."
        $yq_cmd eval -i 'del(.metadata.annotations."kubectl.kubernetes.io/last-applied-configuration")' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.creationTimestamp)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.managedFields)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.resourceVersion)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.selfLink)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.metadata.uid)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.clusterIP)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.clusterIPs)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.externalIPs)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.ports[].nodePort)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.status)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i 'del(.spec.externalTrafficPolicy)' $out_dir/$m-$manifest.yaml
        $yq_cmd eval -i '.spec.type = "ClusterIP"' $out_dir/$m-$manifest.yaml
        if [ -f "$source_dep/$m-deploymentconfig.yaml" ]; then
            cat $source_dep/$m-deploymentconfig.yaml | $yq_cmd '.spec.template.metadata.labels' | sed "s/://g" > tmp/$m-$manifest.yaml
            $yq_cmd eval -i 'del(.spec.selector)' $out_dir/$m-$manifest.yaml
            $yq_cmd eval -i '
            .spec += {
              "selector" : {}
            }
            ' $org_dir/$m-$manifest.yaml
            while IFS= read -r line
            do
                arr=($line)
                export key=${arr[0]}
                export value=${arr[1]}
                $yq_cmd -i eval '.spec.selector += {strenv(key): strenv(value)}' $out_dir/$m-$manifest.yaml
            done< <(cat tmp/$m-$manifest.yaml)
            rm -f tmp/$m-$manifest.yaml
        fi
    done
done