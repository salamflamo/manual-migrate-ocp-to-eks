#!/usr/bin/env bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-deploymentconfig.log"
    exit 1
fi

# global variable
oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=replicationcontroller
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
host_ocp=image-registry.openshift-image-registry.svc:5000
host_ecr=987529536644.dkr.ecr.ap-southeast-3.amazonaws.com

mkdir rc-list -p
rm -f rc-list/$namespaces.txt

if [ "$($oc_cmd version)" ]; then
    echo "oc is exists"
else
    echo "there is no ocp package"
    exit 1
fi

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "please install it"
    exit 1
fi

echo $(date)
echo "From namespace $namespaces .."
if [ ! -d "$org_dir" ]; then
    mkdir $org_dir -p
fi
if [ ! -d "$out_dir" ]; then
    mkdir $out_dir -p
fi

for rcfile in $(ls source/$namespaces/deploymentconfig);do
    rc=$(echo $rcfile | sed "s/-deploymentconfig.yaml//g")
    echo $rc >> rc-list/$namespaces.txt
done
rc_file=rc-list/$namespaces.txt

for rc in $(cat $rc_file);do
    $oc_cmd get rc -n $namespaces --no-headers | grep "$rc" | awk '{print $1}' > rc-$rc-list.txt
    for rr in $(cat rc-$rc-list.txt);do
       echo "Collecting manifest $manifest $rc"

        ver=$($oc_cmd get rc $rr  -n $namespaces -o yaml | yq '.metadata.annotations."openshift.io/deployment-config.latest-version"')
        echo "$ver $rr" >> tmp-$rc.txt
    done

    for r in $(cat tmp-$rc.txt | sort -k1 -n -r | head -3 | grep -v "null" | awk '{print $2}');do
        $oc_cmd get $manifest $r -n $namespaces -o yaml > $org_dir/$r-$manifest.yaml

        #section convert
        echo "Converting manifest $manifest $file.."
        file=$r-$manifest.yaml
        cp $org_dir/$file $out_dir/$file
        VR=`cat $org_dir/$file | $yq_cmd '.metadata.annotations."openshift.io/deployment-config.latest-version"'`
        export VERSION=$(($VR+10))
        export DC=`cat $org_dir/$file | $yq_cmd '.metadata.ownerReferences[0].name'`
        export REPLICAS=`cat $org_dir/$file | $yq_cmd '.spec.replicas'`
        export UID_DC=`cat source/$namespaces/deploymentconfig/$DC-deploymentconfig.yaml | $yq_cmd '.metadata.uid'`

        #delete section
        $yq_cmd eval -i 'del(.metadata.annotations."kubectl.kubernetes.io/last-applied-configuration")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.annotations."kubectl.kubernetes.io/original-replicas")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.annotations."openshift.io/deployer-pod.name")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.annotations."openshift.io/deployment.phase")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.annotations."openshift.io/deployment.replicas")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.annotations."openshift.io/deployment.status-reason")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.annotations."openshift.io/encoded-deployment-config")' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.creationTimestamp)' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.generation)' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.managedFields)' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.resourceVersion)' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.selfLink)' $out_dir/$file
        $yq_cmd eval -i 'del(.metadata.uid)' $out_dir/$file
        $yq_cmd eval -i 'del(.status)' $out_dir/$file
        $yq_cmd eval -i '.spec.replicas = 0' $out_dir/$file
        $yq_cmd eval -i 'del(.spec.template.spec.nodeSelector)' $out_dir/$file
        $yq_cmd eval -i '.spec.template.spec += {"nodeSelector": {"Role" : "Workload"}}' $out_dir/$file
        #modify section
        $yq_cmd eval -i '.apiVersion = "apps/v1"' $out_dir/$file
        $yq_cmd eval -i '.kind = "ReplicaSet"' $out_dir/$file
        $yq_cmd eval -i '.metadata.annotations +=
            {
                "deployment.kubernetes.io/desired-replicas" : strenv(REPLICAS),
                "deployment.kubernetes.io/max-replicas" : "3",
                "deployment.kubernetes.io/revision" : strenv(VERSION)
            }' $out_dir/$file
        $yq_cmd eval -i '.metadata.ownerReferences[0].apiVersion = "apps/v1"' $out_dir/$file
        $yq_cmd eval -i '.metadata.ownerReferences[0].kind = "Deployment"' $out_dir/$file
        $yq_cmd eval -i '.metadata.ownerReferences[0].uid = strenv(UID_DC)' $out_dir/$file
        $yq_cmd eval -i '.spec.selector = { "matchLabels" : .spec.selector }' $out_dir/$file
        sed -i "s/$host_ocp/$host_ecr/g" $out_dir/$file
    done
    rm -f tmp-$rc.txt
    rm -f rc-$rc-list.txt
done