#!/usr/bin/env bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba file-rc | tee -a ns-coba-deploymentconfig.log"
    exit 1
fi


oc_cmd=./oc
yq_cmd=./yq
namespaces=$1
manifest=replicationcontroller
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest
dcdir=destination/$namespaces/deploymentconfig

if [ ! -f "$yq_cmd" ]; then
    echo "yq is not exists"
    echo "yq installing ....."
    wget https://github.com/mikefarah/yq/releases/download/v4.23.1/yq_linux_amd64.tar.gz
    tar -zxvf yq_linux_amd64.tar.gz;mv yq_linux_amd64 yq
fi

echo $(date)
echo "From namespace $namespaces .."

for rc in $(ls $out_dir);do
    dc=$(cat $out_dir/$rc | $yq_cmd '.metadata.ownerReferences[0].name')
    echo "Processing RC $out_dir/$rc with DC $dc"
    export uid_dc=$(kubectl get deployment $dc -n $namespaces -o yaml | $yq_cmd '.metadata.uid')
    $yq_cmd eval -i '.metadata.ownerReferences[0].uid = strenv(uid_dc)' $out_dir/$rc
    $yq_cmd eval -i '.metadata.annotations."deployment.kubernetes.io/desired-replicas" = "0"' $out_dir/$rc
    $yq_cmd eval -i '.metadata.annotations."deployment.kubernetes.io/max-replicas" = "0"' $out_dir/$rc
done