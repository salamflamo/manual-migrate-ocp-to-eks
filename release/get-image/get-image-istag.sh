#!/bin/bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-download-image.log"
    exit 1
fi
user_ocp=22343102
namespace=$1
host_ocp=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id
host=image-registry.openshift-image-registry.svc:5000
host_ecr=670012318318.dkr.ecr.ap-southeast-3.amazonaws.com

mkdir -p tmp/is
oc get istag -n $namespace --no-headers | awk '{print $1,$2}' > tmp/is/list-is-$namespace-istag.txt
file=tmp/is/list-is-$namespace-istag.txt
while IFS= read -r line
do
        arr=($line)
        img=${arr[0]}
        img_shasum=${arr[1]}
        ecr=$host_ecr/$namespace/$img
        ocp=$(echo ${img_shasum/$host/$host_ocp})
        img_name=$(echo $img | cut -d: -f 1)
        aws ecr create-repository --repository $namespace/$img_name --image-scanning-configuration scanOnPush=true
        sudo docker login -u $user_ocp -p $(oc whoami -t) $host_ocp
        sudo docker pull $ocp
        sudo docker tag $ocp $ecr
        aws ecr get-login-password --region ap-southeast-3 | sudo docker login --username AWS --password-stdin $host_ecr
        sudo docker push $ecr
        sudo docker rmi $ecr
        # sudo docker images | grep $img_name | awk '{print $3}' | xargs sudo docker rmi --force
        echo "+++++++++++++++++++++++++"
done< <(cat $file)