#!/bin/bash
if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba | tee -a ns-coba-download-image.log"
    exit 1
fi
user_ocp=22343102
namespace=$1
host_ocp=default-route-openshift-image-registry.apps.ocp-nprod.telkomsel.co.id
host_ecr=670012318318.dkr.ecr.ap-southeast-3.amazonaws.com
mkdir -p tmp/is

oc get is -n $namespace --no-headers | awk '{print $3}' > tmp/is/list-is-$namespace-tag.txt
oc get is -n $namespace --no-headers | awk '{print $2}' > tmp/is/list-is-$namespace-img.txt
file_prefix=tmp/is/list-is-$namespace

tot=$(cat $file_prefix-img.txt | wc -l)

for ((j=1;j<$tot;j++));do
        img=$(cat $file_prefix-img.txt | head -$j | tail -1)
        img_name=$(echo $img | cut -d/ -f 3)
        date
        aws ecr create-repository --repository $namespace/$img_name --image-scanning-configuration scanOnPush=true
        tag=$(cat $file_prefix-tag.txt | head -$j | tail -1)
        t=${tag/,/ }
        arr=($t)
        len=${#arr[@]}
        for ((i=0;i<$len;i++));do
                ecr=$(echo ${img/$host_ocp/$host_ecr}):${arr[$i]}
                ocp=$img:${arr[$i]}
                sudo docker login -u $user_ocp -p $(oc whoami -t) $host_ocp
                sudo docker pull $ocp
                sudo docker tag $ocp $ecr
                aws ecr get-login-password --region ap-southeast-3 | sudo docker login --username AWS --password-stdin $host_ecr
                sudo docker push $ecr
                i_name=$(echo $ecr | cut -d/ -f 3 | cut -d: -f 1)
                sudo docker rmi $ecr
                # sudo docker images | grep $i_name | awk '{print $3}' | xargs sudo docker rmi --force
                echo "++++++++++++++++++++++"
        done
done
