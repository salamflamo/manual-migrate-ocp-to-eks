#!/usr/bin/env bash

if [ -z $1 ]; then
    echo "What namespace dude?, look at the example $0 ns-coba"
    exit 1
fi

oc_cmd=./oc
yq_cmd=./yq
kube_cmd=./kubectl
namespaces=$1
manifest=service
org_dir=source/$namespaces/$manifest
out_dir=destination/$namespaces/$manifest

mkdir $org_dir -p
mkdir $out_dir -p
$oc_cmd get $manifest -n $namespaces -o yaml | $yq_cmd '.items[].metadata.name' | tee -a $out_dir/list-$manifest.txt